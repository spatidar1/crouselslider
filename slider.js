/* eslint-disable react-native/no-inline-styles */
import React, {useRef, useEffect, useState} from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  SafeAreaView,
  Animated,
} from 'react-native';
import ExpandingDot from './Expanded';
const Slider = ({allowScroll, currentIndex, Index}) => {
  //console.log('Render ', allowScroll, currentIndex);
  const link = [
    'https://image.shutterstock.com/image-vector/online-exam-computer-web-app-260nw-1105800884.jpg',
    'https://www.automatetheplanet.com/wp-content/uploads/2015/06/Test-URL-Redirects-WebDriver.jpg',
    'https://image.shutterstock.com/image-vector/online-exam-computer-web-app-260nw-1105800884.jpg',
    'https://www.automatetheplanet.com/wp-content/uploads/2015/06/Test-URL-Redirects-WebDriver.jpg',
    'https://image.shutterstock.com/image-vector/online-exam-computer-web-app-260nw-1105800884.jpg',
  ];

  const [index, setIndex] = useState(0);
  const [autoScroll, setAutoScroll] = useState(false);
  const ref = useRef(0);
  const viewConfigRef = useRef({viewAreaCoveragePercentThreshold: 50});
  const scrollX = useRef(new Animated.Value(0)).current;
  const onViewableItemsChanged = React.useCallback(viewableItems => {
    //setIndex(viewableItems.changed[0].index);
  }, []);

  useEffect(() => {
    if (allowScroll) {
      setAutoScroll(true);
    } else {
      setAutoScroll(false);
    }
    if (!autoScroll) {
      return;
    }
    const timer = setTimeout(() => {
      let current = index;
      if (current === link.length) {
        current = 0;
      }
      ref?.current?.scrollToIndex({animated: true, index: current});
      setIndex(current + 1);
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, [index, autoScroll, link]);
  return (
    <SafeAreaView
      style={{
        flex: 1,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        height: 400,
        width: 350,
      }}>
      <FlatList
        ref={ref}
        data={link}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {
            useNativeDriver: false,
          },
        )}
        // onMomentumScrollEnd={() => setAutoScroll(true)}
        onScrollBeginDrag={() => setAutoScroll(false)}
        horizontal
        onViewableItemsChanged={onViewableItemsChanged}
        viewabilityConfig={viewConfigRef.current}
        keyExtractor={(item, index) => item + index}
        renderItem={({item, index}) => {
          return (
            <View
              style={{alignItems: 'center'}}
              onTouchMove={e => {
                //console.log(e);
              }}>
              <Image source={{uri: item}} style={{height: 600, width: 350}} />
              <Text>{`Image ${index}`}</Text>
            </View>
          );
        }}
      />
      <ExpandingDot
        data={link}
        expandingDotWidth={30}
        scrollX={scrollX}
        inActiveDotOpacity={0.6}
        dotStyle={{
          width: 10,
          height: 10,
          backgroundColor: '#FFFFFF',
          borderRadius: 5,
          marginHorizontal: 5,
        }}
        containerStyle={{
          top: 60,
          height: 15,
          width: 300,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      />
    </SafeAreaView>
  );
};
export default Slider;
