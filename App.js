import React, {useRef, useCallback, useState} from 'react';
import Slider from './slider';
import {FlatList, View} from 'react-native';
const App = () => {
  const [currentIndex, setIndex] = useState(0);
  const viewConfigRef = useRef({viewAreaCoveragePercentThreshold: 90});
  const onViewableItemsChanged = useCallback(viewableItems => {
    console.log('Index : ', viewableItems.changed[0].index);
    setIndex(viewableItems.changed[0].index);
  }, []);
  const viewPairs = useRef([
    {
      viewabilityConfig: viewConfigRef.current,
      onViewableItemsChanged: onViewableItemsChanged,
    },
  ]);
  return (
    <FlatList
      keyExtractor={index => index}
      data={[1, 2, 3, 4, 5, 6]}
      renderItem={({index}) => {
        return (
          <View style={{marginVertical: 20}}>
            {/* <Slider /> */}
            <Slider allowScroll={currentIndex === index} />
          </View>
        );
      }}
      viewabilityConfigCallbackPairs={viewPairs.current}
    />
  );
};

export default App;
