import React from 'react';
import {View, Dimensions, Animated, StyleSheet, ViewStyle} from 'react-native';

const {width} = Dimensions.get('screen');
const ExpandingDot = ({
  scrollX,
  data,
  dotStyle,
  containerStyle,
  inActiveDotOpacity,
  inActiveDotColor,
  expandingDotWidth,
}) => {
  const defaultProps = {
    inActiveDotColor:
      inActiveDotColor || styles.dotStyle.backgroundColor.toString(),
    inActiveDotOpacity: inActiveDotOpacity || 0.5,
    expandingDotWidth: expandingDotWidth || 20,
    dotWidth: dotStyle.width || 10,
  };

  return (
    <View style={[styles.containerStyle, containerStyle]}>
      {data.map((_, index) => {
        const inputRange = [
          (index - 1) * (width - defaultProps.expandingDotWidth),
          index * (width - defaultProps.expandingDotWidth),
          (index + 1) * (width - defaultProps.expandingDotWidth),
        ];

        const s =
          dotStyle.backgroundColor !== undefined ? dotStyle : styles.dotStyle;

        const colour = scrollX.interpolate({
          inputRange,
          outputRange: [
            defaultProps.inActiveDotColor,
            s.backgroundColor.toString(),
            defaultProps.inActiveDotColor,
          ],
          extrapolate: 'clamp',
        });
        const opacity = scrollX.interpolate({
          inputRange,
          outputRange: [
            defaultProps.inActiveDotOpacity,
            1,
            defaultProps.inActiveDotOpacity,
          ],
          extrapolate: 'clamp',
        });
        const expand = scrollX.interpolate({
          inputRange,
          outputRange: [
            defaultProps.dotWidth,
            defaultProps.expandingDotWidth,
            defaultProps.dotWidth,
          ],
          extrapolate: 'clamp',
        });

        return (
          <Animated.View
            key={`dot-${index}`}
            style={[styles.dotStyle, dotStyle, {width: expand}, {opacity}]}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    position: 'absolute',
    bottom: 20,
    flexDirection: 'row',
  },
  dotStyle: {
    width: 10,
    height: 10,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    marginHorizontal: 5,
  },
});

export default ExpandingDot;
